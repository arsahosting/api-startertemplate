<?php

namespace handler;

use arsahosting\phpcore\system\controllers\apphandler\HttpHandler;
use arsahosting\phpcore\utils\Util;

class ApiHandler extends HttpHandler {

    function __construct() {
        global $_JSON, $_OJSON, $_LOJSON, $_CREQUEST, $_LCREQUEST;
        $rawBody = file_get_contents('php://input');

        $_JSON = json_decode($rawBody, TRUE); // convert JSON into array
        $_OJSON = json_decode($rawBody); // convert JSON into object
        $_LOJSON = Util::lowercaseObjectIndexes($_OJSON);
        $_CREQUEST = ($_JSON) ? array_merge($_REQUEST, $_JSON) : $_REQUEST;
        $_LCREQUEST = Util::lowercaseObjectIndexes($_CREQUEST);

        parent::__construct();
    }

}
