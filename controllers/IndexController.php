<?php

namespace controllers;

if (! defined("FNAPI"))
	die("This file can not be accesed directly");

class IndexController extends BaseController {
	protected $model;

	public function __CONSTRUCT($controller, $method) {
		parent::__CONSTRUCT($controller, $method);
	}

	public function Index() {
		parent::jsonResponse(array(
			"Hola" => "Mundo"
		));
	}

	public function Headers() {
		parent::jsonResponse(apache_request_headers());
	}

	public function Test() {
		global $_CREQUEST;
		parent::jsonResponse($_CREQUEST);
	}
	
}
