<?php

namespace controllers;

use Exception;
// use models\database\custom\Token;
// use models\database\custom\Usuario;

if (! defined("FNAPI"))
	die("This file can not be accesed directly");

class BaseController {
	private $childController;
	private $childMethod;
	protected $model;
	protected $isLoggedIn = FALSE;
	protected $user = null;

	public function __CONSTRUCT($childController, $childMethod) {
		$this->childController = $childController;
		$this->childMethod = $childMethod;
		
		$this->checkAutho();
	}

	public function Index() {
		throw new Exception("Not yet implemented");
	}

	protected function requireLogin() {
		if (! $this->isLoggedIn) {
			throw new Exception('4');
		}
	}

	protected function requireRole($role) {
		self::requireLogin();
		
		if (!is_array($role)) $role = array($role);
		$role[] = "ROLE_PROGRAMMER";
		$fAllowed = false;
		foreach ($role as $rol) if ($this->user->Role == $rol) {
			$fAllowed = true;
		}
		if (!$fAllowed) throw new Exception('You dont have permission to access this', 403);
	}
	
	protected function requireUserId(int $id) {
		if ($this->user->Id != $id) {
			throw new Exception('You dont have permission to access this', 403);
		}
	}

	protected function jsonResponseIfResponse($response, $warningid = false) {
		if ($response) {
			self::jsonResponse($response, $warningid);
		} else {
			throw new \Exception('There is no results from system', 404);
		}
	}
	
	protected function jsonResponse($response, $warningid = false) {
		header('Content-Type: application/json');
		
		global $warning;
		global $vapi;
		
		$json = array(
			'API_Version' => $vapi
		);
		$json["Success"] = true;
		$json["Warning"] = false;
		
		if ($warningid) {
			$json["Warning"] = true;
			$json["Notice"] = array(
				'Number' => $warningid,
				'Message' => (isset($warning[$warningid])) ? $warning[$warningid] : "Warning desconocido"
			);
		}
		
		$json[ucwords($this->childController) . "/" . ucwords($this->childMethod)] = $response;
		
		ini_set("precision", 14);
		ini_set("serialize_precision", -1);
		
		echo json_encode($json);
	}

	protected function getIdentity() {
		return [
			"Controller" => $this->childController,
			"Method" => $this->childMethod
		];
	}
	
	protected function requireInput($haystack, $needle) {
		if (is_array($needle)) {
			foreach ($needle as $peak) {
				self::requireInput($haystack, $peak);
			}
		} else {
			if (is_array($haystack)) {
				if (!array_key_exists($needle, $haystack)) throw new Exception("You must provide '$needle'", 201);				
			} else {
				if (!property_exists($haystack, $needle)) throw new Exception("You must provide '$needle'", 201);
			}
		}
	}
	
	protected function requireValidInput($haystack, $needle) {
		if (is_array($needle)) {
			foreach ($needle as $peak) {
				self::requireValidInput($haystack, $peak);
			}
		} else {
			if (!isValid($haystack, $needle)) throw new Exception("You must provide '$needle'", 201);			
		}
	}
	
	protected function dbDateFormat($date) {
		return date(
			'Y-m-d H:i:s', 
			strtotime(str_replace(["T","Z"], [" ",""],$date))
		);
	}
	
	private function checkAutho() {
// 		$autho = $this->getAutho();
// 		if ($autho) {
			
// 			$ip = $_SERVER['REMOTE_ADDR'];
// 			$timestamp = date("Y-m-d H:i:s", time());
			
// 			$token = Token::GetDao()->GetByHash($autho);
			
// 			if ($token && $token->IpAllowed == $ip && $token->Expiration > $timestamp) {
// 				// TODO: Renew token if global config var is true
// 				$this->isLoggedIn = TRUE;
// 				$this->user = Usuario::GetDao()->GetById($token->Id_User);
// 			}
// 		}
	}

	private function getAutho() {
		return isValid(lowercaseArrayIndexes(apache_request_headers()), 'autho');
	}
}